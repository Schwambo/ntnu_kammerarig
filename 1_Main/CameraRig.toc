\contentsline {section}{\hbox to\@tempdima {\hfil }List of Figures}{ii}{section*.4}
\contentsline {section}{\numberline {1}Camera Rig, Prototype}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}\ac {cad}, File}{1}{subsection.1.1}
\contentsline {section}{\numberline {2}Battery for the camera rig}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Power supply for the Raspberry Pi}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Battery}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Optimize calculation power}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Enhance computing power\\ through a cluster of \acf {sbc} ?}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Question to ask:}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Cluster?}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Alternative to a cluster}{5}{subsection.3.4}
\contentsline {section}{\numberline {4}LED Bar}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}How to calculate the power consumption of all LEDs in total, if you create your own LED bar?}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}How to dim the LEDs}{8}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Analog:}{8}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Digital:}{8}{subsubsection.4.2.2}
\contentsline {subsection}{\numberline {4.3}Evualation }{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}LED Bar suggestion}{10}{subsection.4.4}
\contentsline {section}{\numberline {5}Economic Evaluation}{10}{section.5}
\contentsline {section}{\hbox to\@tempdima {\hfil }Glossar of definition}{11}{section*.14}
\contentsline {section}{\hbox to\@tempdima {\hfil }Glossar of acronyms}{11}{section*.16}
\contentsline {section}{\hbox to\@tempdima {\hfil }References}{11}{section*.17}
